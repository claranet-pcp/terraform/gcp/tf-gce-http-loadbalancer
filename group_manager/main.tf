resource "google_compute_instance_group_manager" "instance_group_manager" {
  count              = "${length(var.zones)}"
  name               = "${var.service}${count.index}"
  instance_template  = "${var.instance_template}"
  base_instance_name = "${var.service}${count.index}"
  update_strategy    = "NONE"
  zone               = "${element(var.zones, count.index)}"
}
