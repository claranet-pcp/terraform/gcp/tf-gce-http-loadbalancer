# tf-gce-instance-group
## variables
* `service` - service name which is going to be running on those instances, will
  be used for hostname, tagging etc.
* `zones` - list of zones in which group managers need to be creted
* `instance_template` -

## Outputs
* `instance_group` -
* `link` -

## Usage example
```
module "sso-group-manager" {
  source = "git::ssh://git@gogs.bashton.net/Bashton/tf-gce-instance-group-manager.git"

  service           = "sso"
  instance_template = "${module.sso-instance-template.link-subnets}"
  zones             = ["${split(",", lookup(var.gcp_zones, var.gcp_region))}"]
}
```
