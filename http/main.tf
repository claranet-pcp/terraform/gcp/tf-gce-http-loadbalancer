resource "google_compute_firewall" "http_lb_firewall" {
  name    = "http-lb-firewall-${var.service}"
  network = "${var.network_name}"

  allow {
    protocol = "tcp"
    ports    = ["80"]
  }

  source_ranges = ["130.211.0.0/22", "35.191.0.0/16"]
	target_tags 	= ["${var.service}"]
}

module "http_lb_intance_template" {
  source = "../instance_template"

  machine_type      = "${var.machine_type}"
  preemptible       = "${var.preemptible}"
  automatic_restart = "${var.automatic_restart}"
  envname           = "${var.envname}"
  service           = "${var.service}"
  domain            = "${var.dns_zone_name}"
  fw_tags           = ["${var.service_tags}"]
  needs_nat         = "${var.natting_tag}"
  startup_script    = "${var.bootup_script}"
  disk_image        = "${var.gce_iamge_family}"
  net_name          = "${var.network_name}"
  ip_forward        = "${var.natting_tag == "natted" ? false : true}"
  gcp_region        = "${var.gcp_region}"
  scopes            = ["${var.iam_scopes}"]
}

module "http_lb_instance_group" {
  source = "../group_manager"

  service           = "${var.service}-lb"
  instance_template = "${module.http_lb_intance_template.link}"
  zones             = ["${var.gcp_zones}"]
}

resource "google_compute_backend_service" "main" {
  name        = "${var.service}-proxy-main-backends"
  port_name   = "${var.lb_backends_port_name}"
  protocol    = "${var.lb_backends_protocol}"
  timeout_sec = "${var.lb_backends_timeout}"
  enable_cdn  = "${var.enable_cdn}"

  backend {
    group = "${module.http_lb_instance_group.instance_group_list[0]}"
  }
  backend {
    group = "${var.lb_zones_count >= 2 ? module.http_lb_instance_group.instance_group_list[1] : module.http_lb_instance_group.instance_group_list[0]}"
  }
  backend {
    group = "${var.lb_zones_count == 3 ? module.http_lb_instance_group.instance_group_list[2] : module.http_lb_instance_group.instance_group_list[0]}"
  }
  health_checks = ["${google_compute_http_health_check.default.self_link}"]
}

resource "google_compute_http_health_check" "default" {
  name               = "${var.service}-proxy-health-check"
  request_path       = "${var.health_check_req_path}"
  check_interval_sec = "${var.health_check_interval}"
  timeout_sec        = "${var.health_check_timeout}"
}

resource "google_compute_url_map" "default" {
  name            = "${var.service}-proxy-url-map"
  default_service = "${google_compute_backend_service.main.self_link}"

  host_rule {
    hosts        = ["${var.url_map_host_rule_hosts}"]
    path_matcher = "${var.url_map_host_rule_pathmatch}"
  }

  path_matcher {
    name            = "${var.url_map_host_rule_pathmatch}"
    default_service = "${google_compute_backend_service.main.self_link}"

    path_rule {
      paths   = ["${var.url_map_pathmatch_path_rule}"]
      service = "${google_compute_backend_service.main.self_link}"
    }
  }
}

resource "google_compute_target_http_proxy" "default" {
  name    = "${var.service}-proxy-target"
  url_map = "${google_compute_url_map.default.self_link}"
}

resource "google_compute_global_forwarding_rule" "default" {
  name       = "${var.service}-proxy-forwarding-rule"
  target     = "${google_compute_target_http_proxy.default.self_link}"
  port_range = "${var.forwarding_rule_port_range}"
}
