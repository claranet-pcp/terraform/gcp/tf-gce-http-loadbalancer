variable "gcp_project"   {}
variable "dns_zone_name" {}
variable "service_tags"  {type = "list"}

variable "preemptible"       {default = false}
variable "automatic_restart" {default = true}

variable "gcp_region"   {default = "europe-west1"}
variable "enable_cdn"   {default = false}
variable "envname"      {default = "gcp"}
variable "machine_type" {default = "f1-micro"}
variable "service"      {default = "frontend"}
variable "natting_tag"  {default = "natted"}
variable "network_name" {default = "default"}
variable "bootup_script"    {default = "gs://testing-tf-bootstrap/bootstrap.sh"}
variable "gce_iamge_family" {default = "debian-8"}

variable "lb_zones_count"        {default = 3}
variable "lb_backends_protocol"  {default = "HTTP"}
variable "lb_backends_port_name" {default = "http"}
variable "lb_backends_timeout"   {default = 10}

variable "health_check_req_path" {default = "/"}
variable "health_check_timeout"  {default = 15}
variable "health_check_interval" {default = 30}

variable "url_map_host_rule_hosts"     {default = ["*"]}
variable "url_map_host_rule_pathmatch" {default = "allpaths"}
variable "url_map_pathmatch_path_rule" {default = ["/", "/*"]}

variable "forwarding_rule_port_range" {default  = 80}

variable "iam_scopes" {
  default = [
    "storage-ro",
    "compute-ro"
  ]
}

variable "gcp_zones" {
  default = [
    "europe-west1-b",
    "europe-west1-c",
    "europe-west1-d"
  ]
}
