# tf-gce-http-loadbalancer
This directory contains 2 modules allowing for creating HTTP and/or HTTPS GCP
Load Balancer from a single place.

## Compatibility
This modules have been tested with TF:
0.9.1 (using features from 0.8.0)

## Variables
Variables available for each module are described in the README file in each
respective directory (http and https).

## Outputs
There are no outputs (yet) in here, probably outputing external IP for the
proxy/LB is a good idea

## Usage example
```
module "http_lb" {
  source = "git::ssh://git@gogs.bashton.net:Bashton-Terraform-Modules/tf-gce-http-loadbalancer.git/http"

  gcp_project       = "intrepid-signal-149714"
  dns_zone_name     = "kula.test"
  service_tags      = ["kula", "test", "httploadbalancer"]
  lb_zones_count    = 3
  preemptible       = true
  automatic_restart = false
}

module "https_lb" {
  source = "git::ssh://git@gogs.bashton.net:Bashton-Terraform-Modules/tf-gce-http-loadbalancer.git/https"

  gcp_project                = "intrepid-signal-149714"
  dns_zone_name              = "kula.test"
  service_tags               = ["kula", "test", "httploadbalancer"]
  service                    = "https"
  forwarding_rule_port_range = 443
  lb_zones_count             = 2
  ssl_cert_path              = "./ssl/ssl-cert-snakeoil.pem"
  ssl_key_path               = "./ssl/ssl-cert-snakeoil.key"
  preemptible                = true
  automatic_restart          = false
}

```
