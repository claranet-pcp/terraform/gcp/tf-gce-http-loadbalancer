# Instance template variables
variable "envname"        {}
variable "service"        {}
variable "domain"         {}
variable "startup_script" {}
variable "disk_image"     {}
variable "net_name"       {}
variable "gcp_region"     {}

variable "image_project" {
  default = "debian-cloud"
}

variable "network_enabled" {
  default = 1
}

variable "subnets_enabled" {
  default = 0
}

variable "fw_tags" {
  type = "list"
}

variable "needs_nat" {
  default = "nat"
}

variable "machine_type" {
  default = "g1-small"
}

variable "disk_device_name" {
  default = "sda"
}

variable "scopes" {
  default = [
    "storage-ro",
    "compute-ro"
  ]
}

# Needed for NAT
variable "ip_forward" {
  default = true
}

variable "preemptible" {
  default = false
}

variable "automatic_restart" {
  default = true
}
