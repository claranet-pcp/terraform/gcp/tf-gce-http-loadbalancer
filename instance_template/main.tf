resource "google_compute_instance_template" "instance_template-subnets" {
  lifecycle {
    create_before_destroy = true
    ignore_changes        = ["automatic_restart"]
  }

  name_prefix    = "${var.service}-${var.disk_image}-"
  can_ip_forward = "${var.ip_forward}"
  machine_type   = "${var.machine_type}"
  region         = "${var.gcp_region}"

  tags = [
    "${var.envname}",
    "${var.service}",
    "${var.needs_nat}",
    "${var.fw_tags}",
  ]

  disk {
    device_name  = "${var.disk_device_name}"
    source_image = "https://www.googleapis.com/compute/v1/projects/${var.image_project}/global/images/family/${var.disk_image}"
    boot         = true
  }

  network_interface {
    subnetwork = "${var.net_name}"
  }

  metadata {
    envname            = "${var.envname}"
    profile            = "${var.service}"
    domain             = "${var.domain}"
    startup-script-url = "${var.startup_script}"
  }

  service_account {
    scopes = ["${var.scopes}"]
  }

  scheduling {
    preemptible       = "${var.preemptible}"
    automatic_restart = "${var.automatic_restart}"
  }
}
