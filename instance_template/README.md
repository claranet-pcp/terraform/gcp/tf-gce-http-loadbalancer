# tf-gce-instance-template
Static IPs setup is available on the **static_ip** branch

## Variables
* `envname` -
* `service` -
* `domain` -
* `preemptible` (false) - Should instances created out of this template be
  preemptible or not (automatic_restart can't be true to use this option)
* `automatic_restart` (true) - Have to be **false** to allow **preemptible**
  option to be available (TF default is true)
* `startup_script` -
* `disk_image` -
* `net_name` -
* `network_enabled` (1) - is this template applicable for instances which are
  going to be placed in the single network (no subnets)
* `subnets_enabled` (0) - should this template be assigned to the instances
  which are going to be placed in subnetworks
* `fw_tags` -
* `needs_nat` (nat) -
* `machine_type` (g1-small) -
* `disk_device_name` (sda) -
* `scopes` (list of ["storage-ro", "compute-ro"]) -
* `ip_forward` (true) -
* `image_project` - GCP Project (used for image_family url generation) if using
  your own images it should be your project ID

## Outputs
* `link-net` - outputs instance template link created for network attached
  templates
* `link-subnets` - outputs instance template link created for subnetwork
  attached templates

## Usage examples
```
module "http_lb_intance_template" {
  source = "./instance_template"

  image_project   = "${var.gcp_project}"
  machine_type    = "${var.machine_type}"
  envname         = "${var.envname}"
  service         = "${var.service}"
  domain          = "${var.dns_zone_name}"
  fw_tags         = ["${var.service_tags}"]
  needs_nat       = "${var.natting_tag}"
  startup_script  = "${var.bootup_script}"
  disk_image      = "${var.gce_iamge_family}"
  net_name        = "${var.network_name}"
  ip_forward      = "${var.natting_tag == "natted" ? false : true}"
  gcp_region      = "${var.gcp_region}"
  scopes          = ["${var.iam_scopes}"]
}
```
